// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef FMM_SETTINGS_HH
#define FMM_SETTINGS_HH

// general headers
#include <armadillo>
#include <cassert>
#include <set>

#include "rat/common/log.hh"
#include "rat/common/node.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class Settings> ShSettingsPr;
	enum class RefineStopCriterion {BOTH, EITHER, AVERAGE, AVERAGE2, TIMES, GEOMETRIC_MEAN};
	enum class DirectMode {ALWAYS, TRESHOLD, NEVER};

	// settings struct
	class Settings: virtual public cmn::Node{
		private:
			// always use direct calculation method
			DirectMode direct_ = DirectMode::NEVER;

			// treshold for direct to mlfmm in number of interactions
			fltp direct_tresh_ = 1024*1024;

			// keep multipole memory to avoid 
			// allocation when calling multipole method
			// many times, memory can be deleted manually by
			// calling deallocate_recursively() on the root nodelevel
			bool persistent_memory_ = false;
			
			// use large interaction list
			// this is mathematically more correct
			bool large_ilist_ = false;

			// no allocation in targets (just add fields from previous calculation)
			bool no_allocate_ = true;

			// harmonic settings
			int num_exp_ = 5;

			// tree structure settings
			arma::uword min_levels_ = 3;
			arma::uword max_levels_ = 10;

			// refinement parameters
			fltp num_refine_ = RAT_CONST(128.0); 
			fltp num_refine_min_ = RAT_CONST(5.0);
			fltp size_limit_ = RAT_CONST(0.0);

			// refinement stop criterion
			RefineStopCriterion refine_stop_criterion_ = RefineStopCriterion::GEOMETRIC_MEAN;

			// maximum number of rescales when refining the grid
			arma::uword num_rescale_max_ = 25;
			arma::uword num_subdivide_max_ = 5;

			// interaction list settings
			arma::Col<arma::uword>::fixed<3> morton_weights_ = {1,2,4}; // weighted shifts (must contain a 1,2 and 4)

			// multipole to localpole sorting 
			// this setting has huge impact on performance
			// and is used for developing an optimal m2l kernel
			arma::uword m2l_sorting_ = 0; // 0 = by type, 1 = by source (not implemented), 2 = by target

			// enable gpu computing if available
			// otherwise automatic fallback on CPU occurs
			bool use_m2l_batch_sorting_ = true; // sorts m2l list into optimal kernel executions

			// use source to target GPU kernel
			std::set<int> gpu_devices_{0};
			bool fmm_enable_gpu_ = true;
			bool so2ta_enable_gpu_ = true;
			bool so2mp_enable_gpu_ = false;

			// mlfmm execution settings
			bool enable_s2t_ = true;
			bool enable_fmm_ = true;

			// exectute kernels in separate threads
			bool split_s2t_ = false; // automatically used when using gpu
			bool split_m2l_ = false; // automatically used when using gpu

			// parlalel setup fof the oct-tree grid
			bool parallel_tree_setup_ = true;

			// parallel computing settings
			bool parallel_m2m_ = true;

			// parallel multipole to localpole
			bool parallel_m2l_ = true;

			// parallel localpole to localpole
			bool parallel_l2l_ = true;

			// parallel source to multipole
			bool parallel_s2m_ = true;

			// parallel source to targett
			bool parallel_s2t_ = true;

			// parallel localpole to target
			bool parallel_l2t_ = true;

			// reduce memory use in sacrifice of some speed
			// this sets up the full source to multipole matrix in advance
			// this saves some computation time and could be viable when 
			// a relatively small system needs to be calculated many times
			bool memory_efficient_s2m_ = true;

			// reduce memory use in sacrifice of some speed
			// this sets up the full localpole to target matrix in advance
			// this saves some computation time and could be viable when 
			// a relatively small system needs to be calculated many times
			bool memory_efficient_l2t_ = true;

			// allow subdivision of sources
			bool allow_subdivision_ = true;

		public:
			// constructor
			Settings();

			// destructor
			virtual ~Settings(){};

			// factory
			static ShSettingsPr create();

			// set/get direct mode which determines whether
			// the MLFMM is used or a direct Biot-Savart approach
			void set_direct(const DirectMode direct);
			DirectMode get_direct()const;

			// get/set for treshold to determine under 
			// which number of interactions the Biot-Savart
			// approach is used in favor of the MLFMM
			void set_direct_tresh(const fltp direct_tresh);
			fltp get_direct_tresh() const;

			// get/set for the minimal number of levels
			// used for the oct-tree grid in the MLFMM
			void set_min_levels(const arma::uword min_levels);
			arma::uword get_min_levels() const;

			// set/get for the maximum number of levels
			// use for the oct-tree grid in the MLFMM
			void set_max_levels(const arma::uword min_levels);
			arma::uword get_max_levels() const;

			// set/get for the number of expansions used for the
			// spherical harmonics used in the MLFMM thereby
			// determining the accuracy of the calculation
			// when using single precision floating points
			// the maximum effective value is 8
			void set_num_exp(const int num_exp);
			int get_num_exp()const;
			int get_polesize() const; // calculated from num_exp

			// set/get refinement treshold number
			// is used during the setup of the grid
			// if the average number of sources and/or targets
			// falls below this value the grid refinement
			// is terminated
			void set_num_refine(const fltp num_refine);
			fltp get_num_refine() const;

			// set/get refinement size limit
			void set_size_limit(const fltp size_limit);
			fltp get_size_limit() const;

			// minimum average number of sources and targets 
			// in the grid. If violated the grid dimensions are increased
			// until a good match is found
			void set_num_refine_min(const fltp num_refine_min);
			fltp get_num_refine_min() const;

			// set maximum number fo rescales
			// for when the refinement limit is violated
			void set_num_rescale_max(const arma::uword num_rescale_max);
			arma::uword get_num_rescale_max() const;

			// maximum number of subdivisions
			void set_num_subdivide_max(const arma::uword num_subdivide_max);
			arma::uword get_num_subdivide_max() const;

			// stop critterium for grid refinement
			// determines exactly how refinement treshold is used
			void set_refine_stop_criterion(const RefineStopCriterion refine_stop_criterion);
			RefineStopCriterion get_refine_stop_criterion()const;

			// switches for enabling parallel branches of the MLFMM
			void set_enable_s2t(const bool enable_s2t = true);
			void set_enable_fmm(const bool enable_fmm = true);
			bool get_enable_s2t() const;
			bool get_enable_fmm() const;

			// morton index dimensions should have no effect 
			// weighted shifts (must contain a 1,2 and 4)
			void set_morton_weights(const arma::uword w0, const arma::uword w1, const arma::uword w2);
			void set_morton_weights(const arma::Col<arma::uword>::fixed<3> &weights);
			arma::Col<arma::uword>::fixed<3> get_morton_weights() const;
			arma::uword get_morton_weights(const arma::uword dim) const;

			// set/get for large interaction list
			// when turned on the MLFMM interactions 
			// are separated by a larger distance
			// causing the spherical harmonics 
			// transformations to be more accurated
			// however this comes at the cost of more 
			// interactions
			void set_large_ilist(const bool large_interaction_list = true);
			bool get_large_ilist() const;

			// no allocation in the targets
			// assumes that the field matrices in the
			// targets are already allocated
			void set_no_allocate(const bool no_allocate = true);
			bool get_no_allocate() const;

			// do nott reallocate target field matrices when running 
			// multiple calculations with the same set of sourcees and targets
			void set_persistent_memory(const bool persistent_memory = true);
			bool get_persistent_memory() const;

			// set source to multipole sorting type
			// for the inteeraction list note that for 
			// gpu only the "by type" option is available
			// 0 = by type, 1 = by source (not implemented), 2 = by target
			void set_m2l_sorting(const arma::uword m2l_sorting);
			arma::uword get_m2l_sorting() const;

			// sort m2l interaction list into optimal 
			// batches such that a mminimmum number of 
			// executions is required
			void set_use_m2l_batch_sorting(const bool use_m2l_batch_sorting = true);
			bool get_use_m2l_batch_sorting() const;

			// set whether to run the S2T and the M2L parts
			// in threads separate from the main thread
			// this is done by default when gpu is used
			void set_split_s2t(const bool split_s2t = true);
			void set_split_m2l(const bool split_m2l = true);
			bool get_split_s2t() const;
			bool get_split_m2l() const;

			// enable parallelism in the setup of the MLFMM grid
			void set_parallel_tree_setup(const bool parallel_tree_setup = true);
			bool get_parallel_tree_setup() const;

			// enable parallelism in the multipole to multipole step
			void set_parallel_m2m(const bool parallel_m2m = true);
			bool get_parallel_m2m() const;

			// enable parallelism in the multipole to localpole step
			void set_parallel_m2l(const bool parallel_m2l = true);
			bool get_parallel_m2l() const;

			// enable parallelism in the localpole to localpole step
			void set_parallel_l2l(const bool parallel_l2l = true);
			bool get_parallel_l2l() const;

			// enable parallelism in the source to target step
			void set_parallel_s2t(const bool parallel_s2t = true);
			bool get_parallel_s2t() const;

			// enable parallelism in the source to multipole step
			void set_parallel_s2m(const bool parallel_s2m = true);
			bool get_parallel_s2m() const;

			// enable paralleism in the localpole to target step
			void set_parallel_l2t(const bool parallel_l2t = true);
			bool get_parallel_l2t() const;

			// disable all parallelism this just 
			// sets all parallelism switches to false
			void set_single_threaded();

			// recalculate the source to multipole matrices every
			// step such that they do not need to be stored in memory
			void set_memory_efficient_s2m(const bool memory_efficient_s2m = true);
			bool get_memory_efficient_s2m() const;

			// recalculate the localpole to target matrices every
			// step such that they do not need to be stored in memory
			void set_memory_efficient_l2t(const bool memory_efficient_l2t = true);
			bool get_memory_efficient_l2t() const;

			// enable gpu for mulipole method steps
			void set_fmm_enable_gpu(const bool enable_fmm_gpu = true);
			bool get_fmm_enable_gpu() const;

			// enable gpu for source to target step
			void set_so2ta_enable_gpu(const bool so2ta_enable_gpu = true);
			bool get_so2ta_enable_gpu() const;

			// enable gpu for source to multipole step
			void set_so2mp_enable_gpu(const bool so2mp_enable_gpu = true);
			bool get_so2mp_enable_gpu() const;

			// enable gpu for all possible steps
			void set_enable_gpu(const bool enable_gpu = true);

			// set which devices are available for calculatting
			std::set<int> get_gpu_devices() const;			
			void set_gpu_devices(const std::set<int> &gpu_devices);
			void add_gpu_device(const int device_index);
			void remove_gpu_device(const int device_index);

			// display all settings
			void display(const cmn::ShLogPr &lg) const;

			// validity check
			bool is_valid(bool enable_throws)const override;

			// allow subdivision of mesh type of sources
			void set_allow_subdivision(const bool allow_subdivision);
			bool get_allow_subdivision()const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
