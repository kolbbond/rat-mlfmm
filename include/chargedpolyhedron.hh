// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef FMM_CHARGED_POLYHEDRON_HH
#define FMM_CHARGED_POLYHEDRON_HH

#include <armadillo> 
#include <cassert>
#include <algorithm>
#include <memory>
#include <list>
#include <mutex>

// common headers
#include "rat/common/typedefs.hh"

// code specific to Raccoon
namespace rat{namespace fmm{

	// hb curve class template
	class ChargedPolyhedron{
		// methods
		public:
			// TODO these functions need to move to rat::cmn::Extra
			// // check if points are listed in counter clockwise order
			// static fltp ccw(
			// 	const arma::Col<fltp>::fixed<2>&A,
			// 	const arma::Col<fltp>::fixed<2>&B,
			// 	const arma::Col<fltp>::fixed<2>&C);

			// // check if two edges intersect
			// static bool intersect(
			// 	const arma::Col<fltp>::fixed<2>&A, 
			// 	const arma::Col<fltp>::fixed<2>&B, 
			// 	const arma::Col<fltp>::fixed<2>&C, 
			// 	const arma::Col<fltp>::fixed<2>&D);

			// analytical integral over the right triangle
			static arma::Row<fltp> right_triangle_scalar_potential(
				const arma::Row<fltp>& a, 
				const arma::Row<fltp>& b, 
				const arma::Row<fltp>& c);
			static fltp right_triangle_scalar_potential(
				const fltp a, const fltp b, const fltp c);
			static arma::Mat<fltp> right_triangle_magnetic_field(
				const arma::Row<fltp>& a, 
				const arma::Row<fltp>& b, 
				const arma::Row<fltp>& c);
			
			// check if two polygons intersect
			static bool intersect_polygons(
				const arma::Mat<fltp>&Rn1, 
				const arma::Mat<fltp>&Rn2, 
				const fltp tol = RAT_CONST(1e-15));

			// check if an edge intersects with a polyhedron
			static bool edge2polyhedron(
				const arma::Mat<fltp>&Rn, 
				const arma::Col<fltp>::fixed<2>&Re1, 
				const arma::Col<fltp>::fixed<2>&Re2);

			// calculate the scalar potential of a charged polyhedron defined by vertices Rn at target points Rt the polyhedron must be planar
			static arma::Row<fltp> calc_scalar_potential(
				const arma::Mat<fltp>&Rn, 
				const arma::Mat<fltp>&Rt);

			// calculate the magnetic field of a charged polyhedron defined by vertices Rn at target points Rt the polyhedron must be planar
			static arma::Mat<fltp> calc_magnetic_field(
				const arma::Mat<fltp>&Rn, 
				const arma::Mat<fltp>&Rt);

			// calculate the magnetic field of a charged polyhedron using gauss points (for testing)
			static arma::Mat<fltp> calc_magnetic_field_gauss(
				const arma::Mat<fltp>&Rn, 
				const arma::Mat<fltp>&Rt, 
				const arma::sword num_gauss = 4);

			// magnetic field calculation
			static arma::Mat<fltp> calc_magnetic_field_8node_brick(
				const arma::Mat<fltp>& Rn, 
				const arma::Mat<fltp> &Rt, 
				const arma::uword num_planes);
			static arma::Mat<fltp> calc_magnetic_field_8node_brick_richardson(
				const arma::Mat<fltp>& Rn, 
				const arma::Mat<fltp> &Rt, 
				const arma::uword num_planes);
	};

}}

#endif