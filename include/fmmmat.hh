// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// This file contains the matrices for all multipole and localpole operations

// include guard
#ifndef FMM_FMMMAT_HH
#define FMM_FMMMAT_HH

// general headers
#include <armadillo> 
#include <complex>
#include <cassert>
#include <iostream>
#include <memory>

// specific headers
#include "rat/common/extra.hh"
#include "rat/common/parfor.hh"
#include "rat/common/log.hh"

// multipole type
#include "extra.hh"
#include "spharm.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// matrices for performing translations and transformations 
	// on multipoles and localpoles
	// note that in matrix multiplication: A(BC) = (AB)C

	// shared pointer definitions
	typedef std::shared_ptr<class FmmMat_Mp2Mp> ShFmmMat_Mp2MpPr;
	typedef std::shared_ptr<class FmmMat_Mp2Lp> ShFmmMat_Mp2LpPr;
	typedef std::shared_ptr<class FmmMat_Lp2Lp> ShFmmMat_Lp2LpPr;

	// multipole to multipole translation
	class FmmMat_Mp2Mp{
		// properties
		private:
			int num_exp_ = 0; // number of expansions
			arma::uword num_mat_; // number of matrices
			arma::field<arma::SpMat<std::complex<fltp> > > M_; // collection of sparse matrices

		// methods
		public:
			FmmMat_Mp2Mp();
			FmmMat_Mp2Mp(
				const int num_exp, 
				const arma::Mat<fltp> &dR);
			static ShFmmMat_Mp2MpPr create();
			void set_num_exp(
				const int num_exp);
			void set_num_exp(
				const arma::uword num_exp);
			void calc_matrix(
				const arma::Mat<fltp> &dR);
			arma::SpMat<std::complex<fltp> > get_matrix(
				const arma::uword k) const;
			arma::Mat<std::complex<fltp> > apply(
				const arma::uword k, 
				const arma::Mat<std::complex<fltp> > &Mp) const;
			arma::Mat<std::complex<fltp> > apply(
				const arma::uword k, 
				const arma::Mat<std::complex<fltp> > &Mp, 
				const arma::uword N) const;
			void display() const; // display matrix values in terminal
			void display(const arma::uword k) const; // display matrix values in terminal
			arma::uword get_num_mat() const;
			int get_num_exp() const;
			static int num_nz(
				const int num_exp); // function for calculating the number of non zeros in the matrices
			void display(
				cmn::ShLogPr &lg) const;
	};

	// multipole to localpole distance matrix
	class FmmMat_Mp2Lp_dist{
		private:
			arma::Mat<fltp> M_;
			int num_exp_;

		public:
			// constructor
			FmmMat_Mp2Lp_dist();
			void set_num_exp(
				const int num_exp);
			arma::Mat<fltp> get_matrix() const;
			void setup(
				const arma::uword N);
			arma::Mat<std::complex<fltp> > apply(
				const arma::Mat<std::complex<fltp> >& M) const;
	};

	// multipole to localpole conversion
	class FmmMat_Mp2Lp{
		// properties
		private:
			int num_exp_ = 0; // number of expansions
			arma::uword num_mat_; // number of matrices
			//arma::Mat<fltp> Mdist_; // matrix for changing distance
			arma::Mat<std::complex<fltp> > M_; // collection of dense matrices

		// methods
		public:
			FmmMat_Mp2Lp();
			FmmMat_Mp2Lp(
				const int num_exp, 
				const arma::Mat<fltp> &dR);
			static ShFmmMat_Mp2LpPr create();
			void set_num_exp(
				const int num_exp);
			void set_num_exp(
				const arma::uword num_exp);
			void calc_matrix(
				const arma::Mat<fltp> &dR);
			arma::Mat<std::complex<fltp> > get_matrix() const;
			arma::Mat<std::complex<fltp> > get_matrix(
				const arma::uword k) const;
			arma::Mat<std::complex<fltp> > apply(
				const arma::uword k, 
				const arma::Mat<std::complex<fltp> > &Mp) const;
			arma::Mat<std::complex<fltp> > apply(
				const arma::uword k, 
				const arma::Mat<std::complex<fltp> > &Mp, 
				const FmmMat_Mp2Lp_dist &M) const;
			void display() const; // display matrix values in terminal
			void display(
				const arma::uword k) const; // display matrix values in terminal
			arma::uword get_num_mat() const;
			int get_num_exp() const;
			static arma::uword num_nz(
				const int num_exp);
			void display(cmn::ShLogPr &lg) const;
			static arma::Mat<std::complex<fltp> > calc_direct(
				const arma::Mat<std::complex<fltp> > &Mp, 
				const arma::Mat<fltp> &dR, 
				const int num_exp, 
				const arma::uword num_dim);
	};

	// multipole to multipole translation
	class FmmMat_Lp2Lp{
		// properties
		private:
			int num_exp_ = 0; // number of expansions
			arma::uword num_mat_; // number of matrices
			arma::field<arma::SpMat<std::complex<fltp> > > M_; // collection of sparse matrices

		// methods
		public:
			FmmMat_Lp2Lp();
			FmmMat_Lp2Lp(
				const int num_exp, 
				const arma::Mat<fltp> &dR);
			static ShFmmMat_Lp2LpPr create();
			static ShFmmMat_Lp2LpPr create(const int num_exp, const arma::Mat<fltp> &dR);
			void set_num_exp(const int num_exp);
			void set_num_exp(const arma::uword num_exp);
			void calc_matrix(const arma::Mat<fltp> &dR);
			arma::SpMat<std::complex<fltp> > get_matrix(const arma::uword k) const;
			arma::Mat<std::complex<fltp> > apply(
				const arma::uword k, 
				const arma::Mat<std::complex<fltp> > &Lp) const;
			arma::Mat<std::complex<fltp> > apply(
				const arma::uword k, 
				const arma::Mat<std::complex<fltp> > &Lp, 
				const arma::uword N) const;
			void display() const; // display matrix values in terminal
			void display(const arma::uword k) const; // display matrix values in terminal
			arma::uword get_num_mat() const;
			int get_num_exp() const;
			static int num_nz(
				const int num_exp); // function for calculating the number of non zeros in the matrices
			void display(
				cmn::ShLogPr &lg) const;
	};

}}

#endif










