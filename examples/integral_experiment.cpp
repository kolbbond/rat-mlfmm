// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// specific headers
#include "rat/common/error.hh"
#include "rat/common/elements.hh"
#include "rat/common/extra.hh"

// header files for models
#include "chargedpolyhedron.hh"
#include "savart.hh"

// main
int main(){
	// settings
	const rat::fltp D = RAT_CONST(0.01); // box side length
	const rat::fltp tol = RAT_CONST(1e-4);
	const arma::uword N = 100;
	const rat::fltp dxtrap = RAT_CONST(5e-3);
	const arma::sword num_gauss = 10;
	const rat::fltp radius = D*4;

	// define box corners
	const arma::Col<rat::fltp>::fixed<3> R0 = {-D/2-dxtrap,-D/2,-D/2}; 
	const arma::Col<rat::fltp>::fixed<3> R1 = {+D/2+dxtrap,-D/2,-D/2}; 
	const arma::Col<rat::fltp>::fixed<3> R2 = {+D/2,+D/2,-D/2}; 
	const arma::Col<rat::fltp>::fixed<3> R3 = {-D/2,+D/2,-D/2};
	const arma::Col<rat::fltp>::fixed<3> R4 = {-D/2-dxtrap,-D/2,+D/2}; 
	const arma::Col<rat::fltp>::fixed<3> R5 = {+D/2+dxtrap,-D/2,+D/2}; 
	const arma::Col<rat::fltp>::fixed<3> R6 = {+D/2,+D/2,+D/2}; 
	const arma::Col<rat::fltp>::fixed<3> R7 = {-D/2,+D/2,+D/2}; 

	// assemble matrix with nodes
	arma::Mat<rat::fltp> Rn(3,8);
	Rn.col(0) = R0; Rn.col(1) = R1; Rn.col(2) = R2; Rn.col(3) = R3;
	Rn.col(4) = R4; Rn.col(5) = R5; Rn.col(6) = R6; Rn.col(7) = R7;

	// element
	const arma::Col<arma::uword> nh = {0,1,2,3,4,5,6,7};

	// element current density
	const arma::Col<rat::fltp>::fixed<3> Je{0,0,1e6};

	// create targets on a sphere
	arma::Mat<rat::fltp> Rnsph; arma::Mat<arma::uword> nsph,ssph;
	rat::cmn::Tetrahedron::create_sphere(Rnsph,nsph,ssph,radius,radius/10);
	const arma::Mat<rat::fltp> Rt = Rnsph.cols(arma::unique(arma::vectorise(ssph)));


	// PART 1: Integration with gauss points
	// integrate over volume using gauss points
	const arma::Mat<rat::fltp> gp = rat::cmn::Hexahedron::create_gauss_points(num_gauss);
	const arma::Mat<rat::fltp> Rgq = gp.rows(0,2);
	const arma::Row<rat::fltp> wgq = gp.row(3);

	// jacobian
	const arma::Mat<rat::fltp> Rgc = rat::cmn::Hexahedron::quad2cart(Rn,Rgq);
	const arma::Mat<rat::fltp> dN = rat::cmn::Hexahedron::shape_function_derivative(Rgq);
	const arma::Mat<rat::fltp> J = rat::cmn::Hexahedron::shape_function_jacobian(Rn,dN);
	const arma::Row<rat::fltp> Jdet = rat::cmn::Hexahedron::jacobian2determinant(J);

	// // allocate values
	// arma::Mat<rat::fltp> H1(3,Rt.n_cols);

	// // walk over targets
	// for(arma::uword i=0;i<Rt.n_cols;i++){
	// 	// relative position
	// 	const arma::Mat<rat::fltp> dR = Rgc.each_col() - Rt.col(i);

	// 	// distance
	// 	const arma::Row<rat::fltp> rho = rat::cmn::Extra::vec_norm(dR);

	// 	// third power
	// 	const arma::Row<rat::fltp> rho3 = rho%rho%rho;

	// 	// integrate charge distribution
	// 	H1.col(i) = -arma::sum(dR.each_row()%(wgq%Jdet/rho3),1);
	// }

	// calculate field from gauss points
	const arma::Mat<rat::fltp> H1 = rat::fmm::Savart::calc_I2H(Rgc, arma::repmat(Je,1,Rgq.n_cols).eval().each_row()%(Jdet%wgq), Rt, false);

	// Part 2: Integration using surface integral
	const arma::Mat<rat::fltp> H2 = rat::cmn::Extra::cross(arma::repmat(Je,1,Rt.n_cols),rat::fmm::ChargedPolyhedron::calc_magnetic_field_8node_brick_richardson(Rn,Rt,4))/(4*arma::Datum<rat::fltp>::pi);
	
	std::cout<<arma::join_horiz(H1.t(),H2.t())<<std::endl;



	// std::cout<<(H1/H2).t()<<std::endl;



	// return
	return 0;
}