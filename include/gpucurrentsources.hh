// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef FMM_CURRENT_SOURCES2_HH
#define FMM_CURRENT_SOURCES2_HH

// check if cuda available
#ifdef ENABLE_CUDA_KERNELS

// general headers
#include <armadillo> 
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>
#include <memory>

// common headers
#include "rat/common/error.hh"
#include "rat/common/extra.hh"
#include "rat/common/gauss.hh"

// mlfmm headers
#include "sources.hh"
#include "savart.hh"
#include "stmat.hh"
#include "targets.hh"
#include "settings.hh"

// CUDA specific headers
#include "gpukernels.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class GpuCurrentSources> ShGpuCurrentSourcesPr;
	typedef arma::field<ShGpuCurrentSourcesPr> ShGpuCurrentSourcesPrList;

	// collection of line sources
	// is derived from the sources class
	class GpuCurrentSources: virtual public Sources, virtual public Targets{
		// properties
		protected:
			// number of dimensions
			arma::uword num_dim_ = 3;


			// coordinates in [m]
			cufltp* Rs_ = NULL; // 4XN matrix contains I

			// direction vector in [m]
			cufltp* dRs_ = NULL; // 4XN matrix contains eps

			// number of sources
			arma::uword num_sources_ = 0;


			// target points on GPU
			cufltp* Rt_ = NULL; // 4XN matrix

			// number of target points
			arma::uword num_targets_ = 0;

			// van Lanen kernel for vector potential calculation
			// this kernel approximates the elements as line currents
			// instead as points and results in a more accurate vector
			// potential and magnetic field when the elements are 
			// close to one another
			bool use_van_Lanen_ = true;

			// relative position to multipole for sources
			cufltp* dRmp_ = NULL;

			// relative position to localpole for targets
			arma::Mat<fltp> dRlp_;

			// multipole operation matrices
			StMat_Lp2Ta M_A_; // for vector potential
			StMat_Lp2Ta_Curl M_H_; // for magnetic field

			// stored field
			cufltp* At_ = NULL;
			cufltp* Ht_ = NULL;

			// multithread lock for adding on M
			std::unique_ptr<std::mutex> lock_ = NULL;

			// output
			bool calc_A_;
			bool calc_H_;

		// methods
		public:
			// constructor
			GpuCurrentSources();
			GpuCurrentSources(
				const arma::Mat<fltp> &Rs, 
				const arma::Mat<fltp> &dRs, 
				const arma::Row<fltp> &Is, 
				const arma::Row<fltp> &epss,
				const arma::Mat<fltp> &Rt,
				const bool calc_A = true,
				const bool calc_H = true);

			// factory
			static ShGpuCurrentSourcesPr create();
			static ShGpuCurrentSourcesPr create(
				const arma::Mat<fltp> &Rs, 
				const arma::Mat<fltp> &dRs, 
				const arma::Row<fltp> &Is, 
				const arma::Row<fltp> &epss,
				const arma::Mat<fltp> &Rt,
				const bool calc_A = true,
				const bool calc_H = true);

			// destructor
			~GpuCurrentSources();

			// set the field type
			void set_field_type(
				const std::string &field_type, 
				const arma::Row<arma::uword> &num_dim);
			void set_field_type(
				const char field_type, 
				const arma::uword num_dim);

			// get field
			arma::Mat<fltp> get_field(const char type) const override;
			bool has(const char type) const override;

			// set coordinates and currents of elements
			void set_source_coords(const arma::Mat<fltp> &Rs);

			// set coordinates and currents of elements
			void set_target_coords(const arma::Mat<fltp> &Rt);

			// setting and getting direction vector
			void set_direction(const arma::Mat<fltp> &dRs);

			// setting of the softening factor
			void set_softening(const arma::Row<fltp> &epss);

			// set currents
			void set_currents(const arma::Row<fltp> &Is);

			// set van Lanen kernel
			void set_van_Lanen(const bool use_van_Lanen);
			bool get_van_Lanen() const;

			// get element size
			fltp element_size() const override;

			// sort function
			void sort_sources(const arma::Row<arma::uword> &sort_idx)override;
			void sort_targets(const arma::Row<arma::uword> &sort_idx)override;

			// unsort
			virtual void unsort_sources(const arma::Row<arma::uword> &sort_idx)override;
			virtual void unsort_targets(const arma::Row<arma::uword> &sort_idx)override;

			// getting source coordinates
			arma::Mat<fltp> get_source_coords() const override;
			arma::Mat<fltp> get_target_coords() const override;
			arma::Mat<fltp> get_target_coords(const arma::uword ft, const arma::uword lt) const override;
			arma::Mat<fltp> get_source_direction() const;
			arma::Row<fltp> get_source_currents() const;
			arma::Row<fltp> get_source_softening() const;

			// count number of sources
			arma::uword num_sources() const override;

			// get number of dimensions
			arma::uword get_num_dim() const override;

			// allocation of storage matrix
			void allocate()override;

			// // access 
			// cufltp* access_vector_potential();
			// cufltp* access_magnetic_field();

			// get number of targets
			virtual arma::uword num_targets()const override;

			// setting of calculated field
			virtual void add_field(
				const char type, 
				const arma::Mat<fltp> &Madd, 
				const bool with_lock = false)override;
			virtual void add_field(
				const char type, 
				const arma::Mat<fltp> &Madd,
				const arma::Row<arma::uword> &ft, 
				const arma::Row<arma::uword> &lt, 
				const arma::Row<arma::uword> &ti, 
				const bool with_lock)override;

			// source to multipole
			void setup_source_to_multipole(
				const arma::Mat<fltp> &dR, 
				const ShSettingsPr &stngs) override;
			void source_to_multipole(
				arma::Mat<std::complex<fltp> > &Mp, 
				const arma::Row<arma::uword> &first_source, 
				const arma::Row<arma::uword> &last_source, 
				const ShSettingsPr &stngs) const override;

			// direct field calculation for both A and B
			void calc_direct(
				const ShTargetsPr &tar, 
				const ShSettingsPr &stngs) const override;
			void source_to_target(
				const ShTargetsPr &tar, 
				const arma::Col<arma::uword> &target_list, 
				const arma::field<arma::Col<arma::uword> > &source_list, 
				const arma::Row<arma::uword> &first_source, 
				const arma::Row<arma::uword> &last_source, 
				const arma::Row<arma::uword> &first_target, 
				const arma::Row<arma::uword> &last_target, 
				const ShSettingsPr &stngs) const override;

			// localpole to target virtual functions
			virtual void setup_localpole_to_target(
				const arma::Mat<fltp> &dR, 
				const arma::uword num_dim,
				const ShSettingsPr &stngs)override;
			virtual void localpole_to_target(
				const arma::Mat<std::complex<fltp> > &Lp, 
				const arma::Row<arma::uword> &first_target, 
				const arma::Row<arma::uword> &last_target, 
				const arma::uword num_dim, 
				const ShSettingsPr &stngs)override;

	};

}}

#endif
#endif