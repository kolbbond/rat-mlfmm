// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef FMM_S2T_MAT_HH
#define FMM_S2T_MAT_HH

// general headers
#include <armadillo> 
#include <complex>
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>
#include <memory>

// specific headers
#include "rat/common/extra.hh"
#include "settings.hh"
#include "rat/common/log.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// unique pointer definition
	typedef std::shared_ptr<class S2TMat> ShS2TMatPr;

	// calculates interaction list for multipole to localpole step
	class S2TMat{
		// properties
		private:
			// the matrix itself
			arma::SpMat<fltp> Ms2t_;

		// methods
		public:
			// constructor
			S2TMat();

			// setup function
			void setup(const ShNodeLevelPr& grid, const ShNodeLevelPr& leaf){
				
			}

			// multiplication
			arma::Mat<fltp> apply(const arma::Mat<fltp>&x){
				return Ms2t_*x;
			}
	};

}}

#endif
