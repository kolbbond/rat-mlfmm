// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef FMM_ILIST_HH
#define FMM_ILIST_HH

// general headers
#include <armadillo> 
#include <complex>
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>
#include <memory>

// specific headers
#include "rat/common/extra.hh"
#include "settings.hh"
#include "rat/common/log.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// unique pointer definition
	typedef std::shared_ptr<class IList> ShIListPr;

	// calculates interaction list for multipole to localpole step
	class IList{
		// properties
		private:
			// input settings
			ShSettingsPr stngs_ = NULL;

			// 8 fields each with 6*6*6 - 3*3*3 = 189-types referring to nshift list
			arma::Mat<arma::uword> shift_type_; 

			// approximate list 7*7*7 - 3*3*3 = 316 long xyz-coords
			arma::Mat<arma::sword> approx_nshift_; 
			
			// direct lists 3*3*3 = 27
			arma::Mat<arma::sword> direct_nshift_;

			// positional shift for different box types
			arma::Mat<arma::sword> type_nshift_;

			// timing
			fltp last_setup_time_ = 0;
			fltp life_setup_time_ = 0;

		// methods
		public:
			// constructor
			IList();
			explicit IList(const ShSettingsPr &stngs);
			
			// factory
			static ShIListPr create();
			static ShIListPr create(const ShSettingsPr &stngs);

			// setup lists
			void setup();
			void setup_small();
			void setup_large();
			void set_settings(const ShSettingsPr &stngs);
			arma::Mat<arma::uword> get_approx_shift_type() const;
			arma::Mat<arma::uword> get_approx_shift_type(const arma::uword type) const;
			arma::Mat<arma::sword> get_approx_nshift() const;
			arma::Mat<arma::sword> get_approx_nshift(const arma::uword type) const;
			arma::Mat<arma::sword> get_direct_nshift() const;
			arma::Mat<arma::uword> get_direct_shift_type() const;
			arma::Mat<arma::uword> get_type_nshift() const;
			//arma::Col<arma::uword>::fixed<3> get_weight() const;

			// quantities
			arma::uword get_num_type() const;
			arma::uword get_num_approx() const;
			arma::uword get_num_direct() const;

			// display function
			void display(const cmn::ShLogPr &lg) const;
	};

}}

#endif